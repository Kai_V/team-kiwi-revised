using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerInteract : MonoBehaviour
{

    public GameObject currentInterObj = null;
    public InteractionObject currentInterObjScript = null;
    public Inventory inventory;

    void Update()
    {
        if(Input.GetButtonDown("Interact") && currentInterObj)
        {
            //Check to see if this object is to be stored in inventory
            if (currentInterObjScript.inventory)
            {
                inventory.AddItem(currentInterObj);
            }

            //Check to see if this object can be opened
            if (currentInterObjScript.openable)
            {
                //Check to see if the object is locked
                if (currentInterObjScript.locked)
                {
                    //check to see if we have the object needed to unlock
                    //Search our inventory for the item needed - if found unlock

                    if (inventory.FindItem(currentInterObjScript.itemNeeded))
                    {
                        //We found the item needed
                        currentInterObjScript.locked = false;
                        Debug.Log(currentInterObj.name + " was unlocked");
                        currentInterObjScript.Open();
                    }
                    else
                    {
                        Debug.Log(currentInterObj.name + " was not unlocked");
                    }


                } 
                else 
                {

                    //object is not unlocked - open the object

                    Debug.Log(currentInterObj.name + " is unlocked");
                    currentInterObjScript.Open();
                }
            }

            //Check to see if this object talks and has a message
            if (currentInterObjScript.talks)
            {
                //Tell the object to give  its message
                currentInterObjScript.Talk();

            }

            if (currentInterObjScript.endscene)
            {
                Debug.Log(currentInterObj.name + " changed scenes");
                currentInterObjScript.EndScene();
            }
        }
    }


    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag ("interObject"))
        {
            Debug.Log(other.name);
            currentInterObj = other.gameObject;
            currentInterObjScript = currentInterObj.GetComponent <InteractionObject> ();
        }

    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.CompareTag("interObject"))
        {
            if (other.gameObject == currentInterObj)
            {
                currentInterObj = null;
            }
            
        }
    }
}
