using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class InteractionObject : MonoBehaviour
{

    public bool inventory;  //If true this object can be stored in inventory
    public bool openable; //If true this objec can be opened
    public bool locked; //If true the object is locked
    public bool talks; //if true then the object can talk to the player
    public bool endscene; //if true change the current scene to the end scene

    public GameObject itemNeeded; //item needed in order to interact with this item
    public string message; //the message this object will give the player


    public void DoInteraction()
    {
        //Picked up and put in inventory
        gameObject.SetActive(false);
    }

    public void Open()
    {
        gameObject.SetActive(false);
    }

    public void Talk()
    {
        Debug.Log(message);
    }

    public void EndScene()
    {
        SceneManager.LoadScene(2); 
    }


}
