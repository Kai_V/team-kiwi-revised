using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DialogueEditor;

public class InputManager : MonoBehaviour
{

    // Update is called once per frame
        private void Update()
        {
            if (ConversationManager.Instance != null)
            {
                if (Input.GetKeyDown(KeyCode.A))
                    ConversationManager.Instance.SelectPreviousOption();

                else if (Input.GetKeyDown(KeyCode.S))
                    ConversationManager.Instance.SelectNextOption();

                else if (Input.GetKeyDown(KeyCode.D))
                    ConversationManager.Instance.PressSelectedOption();
            }

        }
    
}
