using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DialogueEditor;

public class PandaCharacter : MonoBehaviour
{

    public NPCConversation myConversation;

    void OnTriggerStay2D(Collider2D other)
    {
        if (other.gameObject.name == "Player")
        {
            if (Input.GetKey(KeyCode.E))
            {
                ConversationManager.Instance.StartConversation(myConversation);
            }
        }
    }

}
